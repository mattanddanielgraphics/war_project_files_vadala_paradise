/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   AmbientLight.h
 * Author: stuetzlec
 *
 * Created on October 27, 2017, 1:30 PM
 */

#ifndef AMBIENTLIGHT_H
#define AMBIENTLIGHT_H

#include <algorithm>
#include <glm/glm.hpp>
#include "Light.hpp";


using glm::vec3;
using std::min;
using std::max;

class AmbientLight : public Light {
public:
    AmbientLight(vec3);
    AmbientLight(const AmbientLight& orig);
    virtual ~AmbientLight();
    
    void connectLightToShader(Shader*);
    void raiseWeight() { weight = min( weight + 0.1, 1.0); }
    void lowerWeight() { weight = max( weight - 0.1, 1.0); }
private:
    double weight = 0.1;
};

#endif /* AMBIENTLIGHT_H */

