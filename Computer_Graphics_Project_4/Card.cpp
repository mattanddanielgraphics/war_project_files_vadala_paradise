/* Matt Paradis
   Card.cpp
   Class for building a playing card with a textured face and back*/
#include "Card.h"
const float PI = acos(-1.0);

/*Constructor for making the card
 *Card takes in the cardType ( the string of the file to use for the card texture ),
 *as well as the value that this card will hold cardType[] must have the path to the 
 *cards floder i.e. "CardTextures/back_of_card.png"
 *card also takes in the starting location of the top left corner of the card (back of the card)
 *and builds the card up from this point card will start off face down
*/
Card::Card(string cardType, int _value, vec3 _startPoint) {
	//create the new face Texture
	faceTex = new Texture(cardType);
	value = _value;
	startPoint = _startPoint;
	createPoints();
	//set the cPoint(the centr point for the card)

	cPoint = getCenterPoint();
}

//TODO: this does not work with updating the position
vec3 Card::getCenterPoint() {
	vec3 centerPoint;

	centerPoint.x = p1.x + (width / 2);
	centerPoint.y = p1.y + (cardDepth / 2);
	centerPoint.z = p1.z - (height / 2);
	return centerPoint;

	//use this to negate translate rotate in place then use this point again to translate to where it was
}


/* Function that uses the starting point and builds all 8 verts for 
   the points to make up the card.
   Function will call buildCard and make the card with the points it 
   just created*/
void Card::createPoints() {
	//back of the card
	p1 = startPoint;											//top left
	p2.x = p1.x + width; p2.y = p1.y ; p2.z = p1.z;			    //top right
	p3.x = p1.x;		 p3.y = p1.y ; p3.z = p1.z - height;	//bot left
	p4.x = p1.x + width; p4.y = p1.y ; p4.z = p1.z - height;	//bot right

	//face of the card
	p5.x = p1.x;		  p5.y = p1.y + cardDepth ; p5.z = p1.z ;		//top left
	p6.x = p1.x + width;  p6.y = p1.y + cardDepth;	p6.z = p1.z ;		//top right
	p7.x = p1.x;		  p7.y = p1.y + cardDepth; p7.z = p1.z - height;//bot left
	p8.x = p1.x + width;  p8.y = p1.y + cardDepth; p8.z = p1.z - height;//bot right

	v1 = new Vertex(p1, vec3(1, 1, 1));
	v1->setNormal(vec3(0, 1, 0));
	v2 = new Vertex(p2, vec3(1, 1, 1));
	v2->setNormal(vec3(0, 1, 0));
	v3 = new Vertex(p3, vec3(1, 1, 1));
	v3->setNormal(vec3(0, 1, 0));
	v4 = new Vertex(p4, vec3(1, 1, 1));
	v4->setNormal(vec3(0, 1, 0));
	v5 = new Vertex(p5, vec3(1, 1, 1));
	v5->setNormal(vec3(0, 1, 0));
	v6 = new Vertex(p6, vec3(1, 1, 1));
	v6->setNormal(vec3(0, 1, 0));
	v7 = new Vertex(p7, vec3(1, 1, 1));
	v7->setNormal(vec3(0, 1, 0));
	v8 = new Vertex(p8, vec3(1, 1, 1));
	v8->setNormal(vec3(0, 1, 0));

	//make the card with points just created
	buildCard();
}

/*Translates the card and will update the location of the center point
  so that when rotating the card it rotates around itself and not where it was built*/
void Card::translateCard(vec3 trans) {
	//update the location of the center point
	cPoint.x = cPoint.x + trans.x; cPoint.y = cPoint.y + trans.y; cPoint.z = cPoint.z + trans.z;
	this->applyTransformation(glm::translate(glm::mat4(1.0f), glm::vec3(trans)));
}

/*Takes in a vector of where the card should move to 
  works by first translating the point to the origin then moving it to the input point*/
void Card::moveCard(vec3 trans) {
	//move the card to the origin
	this->applyTransformation(glm::translate(glm::mat4(1.0f), glm::vec3(-cPoint)));
	//then move to the new point
	cPoint.x = 0; cPoint.y = 0; cPoint.z = 0;
	translateCard(vec3(trans));
}

/*Rotates the card in place around its center point 
  Pre: takes in the radian that you want to rotate by and the vec3 of the axis to rotate on*/
void Card::rotateInPlace(float radian, vec3 axis) {
	//this->moveCard(-cPoint);
	this->applyTransformation(glm::translate(glm::mat4(1.0f), glm::vec3(-cPoint)));
	this->applyTransformation(glm::rotate(glm::mat4(1.0f), (radian), glm::vec3(axis)));
	this->applyTransformation(glm::translate(glm::mat4(1.0f), glm::vec3(cPoint)));
}

/*Flips the playing card 180 degrees*/
void Card::flipCard(vec3 newLocation) {
	this->rotateInPlace((PI), vec3(0.0, 0.0, 1.0));	//flip the card over
}

/*Function to build the squares that will makeup the card */
void Card::buildCard() {

	t1 = new Triangle(v2, v1, v3);
	t2 = new Triangle(v2, v3, v4);
	t3 = new Triangle(v6, v5, v7);
	t4 = new Triangle(v6, v7, v8);

	t5 = new Triangle(v5, v6, v2);
	t6 = new Triangle(v5, v2, v1);
	t7 = new Triangle(v8, v7, v3);
	t8 = new Triangle(v8, v3, v4);

	t9 = new Triangle(v5, v1, v3);
	t10 = new Triangle(v5, v3, v7);
	t11 = new Triangle(v6, v2, v4);
	t12 = new Triangle(v6, v4, v8);

	t1->calculateNormal();
	t2->calculateNormal();
	t3->calculateNormal();
	t4->calculateNormal();
	t5->calculateNormal();
	t6->calculateNormal();
	t7->calculateNormal();
	t8->calculateNormal();
	t9->calculateNormal();
	t10->calculateNormal();
	t11->calculateNormal();
	t12->calculateNormal();


	s1 = new Quad(v2, v1, v3, v4, t1, t2);
	s2 = new Quad(v6, v5, v7, v8, t3, t4);
	s3 = new Quad(v5, v6, v2, v1, t5, t6);
	s4 = new Quad(v8, v7, v3, v4, t7, t8);
	s5 = new Quad(v5, v1, v3, v7, t9, t10);
	s6 = new Quad(v6, v2, v4, v8, t11, t12);
}

//Returns the value of the card
int Card::getValue() {
	return value;
}


/* The draw function */
void Card::draw(Shader* s) {
	//draw the squares of the card with a texture for the face and back of the card
	s1->draw(s, faceTex);	//the face of the card
	s2->draw(s, backTex);	//back of the card
	s3->draw(s, backTex);	//the sides
	s3->draw(s, backTex);
	s4->draw(s, backTex);
	s5->draw(s, backTex);
	s6->draw(s, backTex);
}
	
Card::~Card() {
	delete s1;
	delete s2;
	delete s3;
	delete s4;
	delete s5;
	delete s6;
}

//scale object given a scaler vector
void Card::scale(GLfloat scaler)
{
	model = glm::scale(model, vec3(scaler, scaler, scaler));

}

//translate the object given a movement vector
void Card::translate(vec3 movement)
{
	model = glm::translate(model, movement);
}

//rotate the object given an axis of rotation and degrees of rotation
void Card::rotate(GLfloat degrees, vec3 axis)
{
	model = glm::rotate(model, degrees, axis);
}