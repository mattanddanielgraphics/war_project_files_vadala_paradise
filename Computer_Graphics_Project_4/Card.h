/* Matt Paradis
   Card.h
   header for class Card, class builds the card and draws the card */
#ifndef CARD_H
#define CARD_H

#include <Windows.h>
#include "DrawableObject.h"
#include "Texture.hpp"
#include "Shader.hpp"
#include "Quad.h"
#include <string>
#include <glm\gtc\matrix_transform.hpp>

using namespace std;

class Card : public DrawableObject
{
public:
	Card(string, int, vec3);
	~Card();

	void draw(Shader* );
	void translateCard(vec3);
	void moveCard(vec3);
	void rotateInPlace(float, vec3);
	void flipCard(vec3);
	int getValue();

	void scale(GLfloat);
	void translate(vec3);
	void rotate(GLfloat, vec3);

	vec3 getCenterPoint();

private:
	//helper functions to buld the squares of the card
	void buildCard();
	void createPoints();

	//private fields for the data that the card will hold
	int value;

	/*Factors used to determine how big to make the card used when 
	  creating the points for the card*/
	int width = 10;
	int height = 20;

	//points of the card to create
	vec3 p1;
	vec3 p2;
	vec3 p3;
	vec3 p4;
	vec3 p5;
	vec3 p6;
	vec3 p7;
	vec3 p8;

	Vertex* v1;
	Vertex* v2;
	Vertex* v3;
	Vertex* v4;
	Vertex* v5;
	Vertex* v6;
	Vertex* v7;
	Vertex* v8;

	Triangle* t1;
	Triangle* t2;
	Triangle* t3;
	Triangle* t4;
	Triangle* t5;
	Triangle* t6;
	Triangle* t7;
	Triangle* t8;
	Triangle* t9;
	Triangle* t10;
	Triangle* t11;
	Triangle* t12;



	//textures for the playing card
	Texture* faceTex;
	//Texture* backTex = new Texture("CardTextures/back_of_card.png"); //normal back
	Texture* backTex = new Texture("Cards/cardBack.png");  

	//The thickness of the card
	float cardDepth = 0.05;
	
	//hold the starting point of the card
	vec3 startPoint;
	vec3 cPoint;

	Quad* s1;
	Quad* s2;
	Quad* s3;
	Quad* s4;
	Quad* s5;
	Quad* s6;
};

#endif