/* Matt Paradis
   Deck.cpp
   Deck class that can create a full deck of 52 cards or take in a vector of existing cards
   from an other deck and set this to be its deck*/

#include "Deck.h"


/*Constructor for when you want to build a full deck of 52 cards from scratch*/
Deck::Deck(vec3 _startPoint) {
	srand((time(0)));	//seed used for shuffling 
	startPoint = _startPoint;
	initCards();
}

/*Constructor for making a player deck this constructor takes in a vector of cards already made
  that would come from splitting the deck and giving the player half the cards*/
Deck::Deck(vec3 _startPoint, vector< Card* > cards) {
	srand((time(0)));	//seed used for shuffling 
	startPoint = _startPoint;
	cardDeck = cards;
}

/* Initial creation of the deck used when making the master deck for the game
   makes all 52 cards sets their proper values and adds them to the deck*/
void Deck::initCards() {
	int curCardValue = 1;
	string name;
	vec3 buildPoint = startPoint;	//init the build point to the start point then incriment
	deckHeight = buildPoint.y;

	for (int i = 0; i < 52; i++) {
		string name = "Cards/" + allCardNames[i];
		//build ontop of the previous card based on width of the card
		buildPoint.y = deckHeight;

		//every 4 cards incriment the card's value
		if ((i % 4) == 0) {
			curCardValue++;
		}
		cout << "Loading..." << endl;
		//add the new card to the deck
		addCard(new Card(name, curCardValue, buildPoint));
	}

	//shuffle the deck after making it
	shuffleDeck(vec3(0.0, 0.0, 0.0));
}

/*Adds a new card to the vector of cards (cardDeck) 
  incriments the deckHeight by the thickness of the card*/
void Deck::addCard(Card* c) {
	cardDeck.push_back(c);
	deckHeight += cardDepth;
}

/*Removes the top card from the deck 
  decrements the deckHeight by the thickness of the card*/
void Deck::removeCard() {
	cardDeck.pop_back();
	deckHeight -= cardDepth;
}

/*Return the current height of the deck */
float Deck::getDeckHeight() {
	return deckHeight;
}

/* Returns the size of the cardDeck vector to return the number of cards that are in the deck*/
int Deck::getNumCards() {
	return cardDeck.size();
}

/* Shuffles the deck */
void Deck::shuffleDeck(vec3 deckPosition) {
	//temporary vector of cards
	vector< Card* > tempDeck;
	int randCard;
	GLfloat counter = 0.0;
	

	//while there are still cards in the deck
	while (cardDeck.size() != 0) {
		//pick a random card and add to temp cards
		randCard = rand() % cardDeck.size();
		tempDeck.push_back(cardDeck.at(randCard));
		cardDeck.at(randCard)->moveCard(vec3(deckPosition.x, deckPosition.y
			+ counter, deckPosition.z));

		//remove the card just added to the temp deck
		cardDeck.erase(cardDeck.begin() + randCard);
		counter += 0.05;
	}
	//set the now empty cardDeck to be tempDeck, cards are now effectively Shuffled
	cardDeck = tempDeck;

}

/*Return true if the deck is empty false if it isn't*/
bool Deck::isEmpty() {
	if (cardDeck.size() == 0) {
		deckHeight = 0.0;
		return true;
	}
	return false;
}

/* Returns the card object at i, used for drawing and when picking a card from the deck*/
Card* Deck::getCardAt(int i) {
	return cardDeck[i];
}

/*Clears out the cards from the deck vector*/
void Deck::clearDeck() {
	cardDeck.clear();
	deckHeight = 0.0;
}

/* Returns half of the deck, this is used when the player starts the game and needs half of 
   the deck from the master deck
   Pre: takes in an int either 1 or 2
   Post: 1 retuns the first 26 cards 2 returns the last 26
   could be more efficient*/
vector< Card* > Deck::splitDeck(int side) {
	vector< Card* > halfDeck;
	GLfloat deckHeight = 0.0;

	switch (side)
	{
	case 1:
		//case for player 1 give them the front half of the deck
		for (int i = 0; i < (cardDeck.size() / 2); i++){
			halfDeck.push_back(cardDeck.at(i));
			cardDeck.at(i)->moveCard(vec3(10, 4.2 + deckHeight, 45));
			deckHeight += 0.05;

		}
		break;
	case 2:
		//case for player 2 give them the back half of the deck
		for (int i = (cardDeck.size() / 2); i < cardDeck.size(); i++) {
			halfDeck.push_back(cardDeck.at(i));
			cardDeck.at(i)->moveCard(vec3(70, 4.2 + deckHeight, 45));
			deckHeight += 0.05;
		}
		break;
	default:
		break;
	}

	return halfDeck;
}

Deck::~Deck() {
}
