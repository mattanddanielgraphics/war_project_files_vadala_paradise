/* Matt Paradis
   Deck.h
   Class to act as a deck of cards using the card class
*/
#ifndef DECK_H
#define DECK_H


#include "Card.h"
#include <vector>
#include <string>
#include <ctime>

using std::vector;
using glm::vec3;
using namespace std;

class Deck 
{
public:
	Deck(vec3);
	Deck(vec3, vector< Card* >);
	~Deck();
	Card* getCardAt(int);

	vector< Card* > splitDeck(int);
	void shuffleDeck(vec3);
	void addCard(Card*);
	void removeCard();
	void clearDeck();
	int getNumCards();
	float getDeckHeight();
	bool isEmpty();

private:
	//helper function to make all the cards of the deck
	void initCards(); 

	vector< Card* > cardDeck;

	//The thickness of the card
	float cardDepth = 0.05;
	vec3 startPoint;

	/*Height of the deck, determines height to place the next card on top*/
	float deckHeight = 0.0;


	//all of the names for the cards  used for creating the deck using a loop
	string allCardNames[52] = 
	{ "ace_of_spades.png", "ace_of_clubs.png" , "ace_of_diamonds.png" , "ace_of_hearts.png",
	  "2_of_spades.png", "2_of_clubs.png", "2_of_diamonds.png", "2_of_hearts.png",
	  "3_of_spades.png", "3_of_clubs.png", "3_of_diamonds.png", "3_of_hearts.png",
	  "4_of_spades.png", "4_of_clubs.png", "4_of_diamonds.png", "4_of_hearts.png", 
	  "5_of_spades.png", "5_of_clubs.png", "5_of_diamonds.png", "5_of_hearts.png",
	  "6_of_spades.png", "6_of_clubs.png", "6_of_diamonds.png", "6_of_hearts.png",
	  "7_of_spades.png", "7_of_clubs.png", "7_of_diamonds.png", "7_of_hearts.png",
      "8_of_spades.png", "8_of_clubs.png", "8_of_diamonds.png", "8_of_hearts.png",
	  "9_of_spades.png", "9_of_clubs.png", "9_of_diamonds.png", "9_of_hearts.png",
	  "10_of_spades.png", "10_of_clubs.png", "10_of_diamonds.png", "10_of_hearts.png", 
	  "jack_of_spades.png", "jack_of_clubs.png", "jack_of_diamonds.png", "jack_of_hearts.png",
	  "queen_of_spades.png", "queen_of_clubs.png", "queen_of_diamonds.png", "queen_of_hearts.png",
	  "king_of_spades.png", "king_of_clubs.png", "king_of_diamonds.png", "king_of_hearts.png"
	};
};
#endif
