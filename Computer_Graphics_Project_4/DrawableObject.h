/*Daniel Vadala
* 10/27/17
* Computer Graphics
* Models in Space
* Author: Professor Stuetzle
* DrawableObject,h
* DrawableObject.h created by Professor Stuetzle and modified by Daniel Vadala.  Class is a virtual class setting default functions for objects to inherit
*/

#ifndef DRAWABLEOBJECT_H
#define DRAWABLEOBJECT_H

#include <glm/glm.hpp>
#include "Shader.hpp"
#include "Texture.hpp"

class DrawableObject {
public:

    DrawableObject() {
        model = glm::mat4(1.0f);
    }

    virtual void draw(Shader*) = 0;
	virtual void scale(GLfloat) = 0;
	virtual void translate(glm::vec3) = 0;
	virtual void rotate(GLfloat, glm::vec3) = 0;
	//virtual void select(bool) = 0;

    glm::mat4 getModelMatrix() {
        return model;
    }

    void applyTransformation(glm::mat4 t) {
        model = t * model;
    }
private:
	

protected:
    glm::mat4 model;
	bool isSelected;

};

#endif /* DRAWABLEOBJECT_H */

