/*Daniel Vadala
* 10/27/17
* Computer Graphics
* War
* Game.cpp
* Game class was created based on SFMLApplication class by Professor Stuetzle and modified by Daniel Vadala.
* Game initiates and plays a game of war with two players each with their own cards
*/

#include "Game.h"



Game::Game()
{
}


Game::~Game()
{
}

void Game::setUpGame(int aaValue, // anti-aliasing level 
	int minorVersion, // OpenGL version
	int majorVersion,
	string winTitle, // The title of the window
	int winWidth, // Width and height of the window
	int winHeight
)
{
	
	// Create the window
	sf::ContextSettings settings;
	settings.depthBits = 24;
	settings.antialiasingLevel = aaValue;
	// If you are getting a failure, comment out these lines first:
	settings.majorVersion = majorVersion;
	settings.minorVersion = minorVersion;

	// Create a new SFML window
	window = new sf::Window(sf::VideoMode(winWidth, winHeight),
		winTitle, sf::Style::Default, settings);


	// To be safe, we�re using glew�s experimental stuff.
	glewExperimental = GL_TRUE;
	// Initialize and error check GLEW
	GLenum err = glewInit();
	if (GLEW_OK != err) {
		// If something went wrong, print the error message
		fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
	}



	//enable tests
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_NORMALIZE);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT1);

	glClearDepth(1.0f);

	// Set the time
	clock.restart();
	curTime = clock.getElapsedTime();
	
	
	
	

}

//drawLoop
void Game::playGame()
{
	while (window->isOpen()) {

		eventFunc(*window);
		draw();
		
	}
}

//Draw cards for both players
void Game::playTurn()
{
	cout << "Play Turn" << endl;
	//initial Draw for both players
	//if deck is empty shuffle cards then play card
	if (player1->isDeckEmpty())
	{
		
		player1->shuffle();
		currentCard1 = player1->playCard();
		//move to play area then flip card
		currentCard1->moveCard(player1Play);
		currentCard1->rotateInPlace(PI, vec3(0.0, 0.0, 1.0));
	}
	else
	{
		
		currentCard1 = player1->playCard();
		//move to play area then flip card
		currentCard1->moveCard(player1Play);
		currentCard1->rotateInPlace(PI, vec3(0.0, 0.0, 1.0));
		
		
		
	}
	if (player2->isDeckEmpty())
	{
		player2->shuffle();
		currentCard2 = player2->playCard();
		//move to play area then flip card
		currentCard2->moveCard(player2Play);
		currentCard2->rotateInPlace(PI, vec3(0.0, 0.0, 1.0));
		
	}
	else
	{
		currentCard2 = player2->playCard();
		//move to play area then flip card
		currentCard2->moveCard(player2Play);
		currentCard2->rotateInPlace(PI, vec3(0.0, 0.0, 1.0));
		
		
	}

	
}

//decide who wins and move cards to proper discard pile
void Game::finishTurn()
{
	//check matchup of cards
	if (currentCard1->getValue() > currentCard2->getValue())
	{
		cout << "Player1 wins" << endl;

		player1->discard(currentCard1);
		currentCard1->moveCard(vec3(player1Discard.x, player1Discard.y + player1->getDiscardDeckHeight(), player1Discard.z));
		currentCard1->rotateInPlace(PI, vec3(0.0, 0.0, 1.0));
		player1->discard(currentCard2);
		currentCard2->moveCard(vec3(player1Discard.x, player1Discard.y + player1->getDiscardDeckHeight(), player1Discard.z));
		currentCard2->rotateInPlace(PI, vec3(0.0, 0.0, 1.0));
	}
	//if tied alternate who wins
	else if (currentCard1->getValue() == currentCard2->getValue())
	{
		//tie goes to player1
		if (tieWinner == 0)
		{
			player1->discard(currentCard1);
			currentCard1->moveCard(vec3(player1Discard.x, player1Discard.y + player1->getDiscardDeckHeight(), player1Discard.z));
			currentCard1->rotateInPlace(PI, vec3(0.0, 0.0, 1.0));
			player1->discard(currentCard2);
			currentCard2->moveCard(vec3(player1Discard.x, player1Discard.y + player1->getDiscardDeckHeight(), player1Discard.z));
			currentCard2->rotateInPlace(PI, vec3(0.0, 0.0, 1.0));
		}
		//tie goes to player2
		else
		{
			player2->discard(currentCard1);
			currentCard1->moveCard(vec3(player2Discard.x, player2Discard.y + player2->getDiscardDeckHeight(), player2Discard.z));
			currentCard1->rotateInPlace(PI, vec3(0.0, 0.0, 1.0));
			player2->discard(currentCard2);
			currentCard2->moveCard(vec3(player2Discard.x, player2Discard.y + player2->getDiscardDeckHeight(), player2Discard.z));
			currentCard2->rotateInPlace(PI, vec3(0.0, 0.0, 1.0));
		}
		tieWinner = (tieWinner + 1) % 2;
		


	}
	else
	{
		player2->discard(currentCard1);
		currentCard1->moveCard(vec3(player2Discard.x, player2Discard.y + player2->getDiscardDeckHeight(), player2Discard.z));
		currentCard1->rotateInPlace(PI, vec3(0.0, 0.0, 1.0));
		player2->discard(currentCard2);
		currentCard2->moveCard(vec3(player2Discard.x, player2Discard.y + player2->getDiscardDeckHeight(), player2Discard.z));
		currentCard2->rotateInPlace(PI, vec3(0.0, 0.0, 1.0));
	}
	//update scoreBoards
	scoreBoard->setPlayer1Score(player1->getScore());
	scoreBoard->setPlayer2Score(player2->getScore());
}

//check if game is over
bool Game::isGameOver()
{
	if (player1->isDeckEmpty() && player1->isDiscardsEmpty())
	{
		//scale player picture to show who won
		if (!alreadyScaled)
		{
			player2->scale(2);
			player2->translate(vec3(-30, 0, 0));
			alreadyScaled = true;
		}
			
		return true;
	}
	else if(player2->isDeckEmpty() && player2->isDiscardsEmpty())
	{
		//scale player picture to show who won
		if (!alreadyScaled)
		{
			player1->scale(2);
			alreadyScaled = true;
		}
		return true;
	}
	return false;
}

//add player objects to the game
void Game::addPlayers(Player* p1, Player* p2)
{
	player1 = p1;
	player2 = p2;
}

//add scoreBoard to game
void Game::addScoreBoard(ScoreKeeper * newBoard)
{
	scoreBoard = newBoard;
}



//draw drawable objects
void Game::draw()
{
	// Now, set the "delta time"
	double dt = clock.getElapsedTime().asSeconds() - curTime.asSeconds();
	curTime = clock.getElapsedTime();
	camera->setDeltaTime(dt);

	// Clears the given framebuffer (in this case, color and depth)
	// Could set color to clear to with glClearColor, default is black
	// Also clears the depth buffer, IMPORTANT because we're now in 3D
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	GLint objectColorLoc = shader->GetVariable("objectColor");
	GLint lightColorLoc = shader->GetVariable("objectColor");
	GLint lightPosLoc = shader->GetVariable("lightPos");
	GLint viewPosLoc = shader->GetVariable("viewPos");
	glUniform3f(objectColorLoc, 30, 30, 30);
	glUniform3f(lightColorLoc, 30, 30, 30);
	glUniform3f(lightPosLoc, 30, 30, 30);
	glUniform3f(viewPosLoc, camera->getPosition().x, camera->getPosition().y,
		camera->getPosition().z);

	// Update the camera
	mat4 projectionMatrix = camera->getPerspectiveMatrix();
	mat4 viewMatrix = camera->getViewMatrix();
	mat4 modelMatrix;
	mat4 PVMMatrix;
	mat4 MVMatrix;
	mat4 VMMatrix; // Needed for the normals
	GLint PVMid;
	GLint VMid;
	GLint MVid;
	GLint EyePositionid;
	GLint TMVid;
	GLint Pid;
	EyePositionid = shader->GetVariable("EyePosition");
	shader->SetVector3(EyePositionid, 1, &(camera->getPosition()[0]));
	for (int j = 0; j < lights.size(); j++) {
		// Connect the light to the shader
		lights[j]->connectLightToShader(shader);
	}

	// Draw everything
	for (int i = 0; i < drawables.size(); i++) {
		if (drawables[i]) {
			modelMatrix = drawables[i]->getModelMatrix();
			// Calculate the matrices
			PVMMatrix = projectionMatrix * viewMatrix * modelMatrix;
			VMMatrix = viewMatrix * modelMatrix;
			MVMatrix = modelMatrix * viewMatrix;
			// Connect the matrices to the shader
			PVMid = shader->GetVariable("PVMMatrix");
			VMid = shader->GetVariable("NormalMatrix");
			MVid = shader->GetVariable("MVMatrix");
			TMVid = shader->GetVariable("Modelview");
			Pid = shader->GetVariable("Projection");
			shader->SetMatrix4(Pid, 1, false, &viewMatrix[0][0]);
			shader->SetMatrix4(MVid, 1, false, &MVMatrix[0][0]);
			shader->SetMatrix4(TMVid, 1, false, &MVMatrix[0][0]);
			shader->SetMatrix4(PVMid, 1, false, &PVMMatrix[0][0]);
			shader->SetMatrix3(VMid, 1, false, &VMMatrix[0][0]);
			drawables[i]->draw(shader);
		}
	}

	// Force OpenGL commands to begin execution
	window->display();
}


void Game::addDrawableObject(DrawableObject* o) {
	drawables.push_back(o);
}

void Game::addLight(Light* o) {
	lights.push_back(o);
}