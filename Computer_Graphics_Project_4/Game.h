/*Daniel Vadala
* 10/27/17
* Computer Graphics
* War
* Game.h
* Game class was created based on SFMLApplication class by Professor Stuetzle and modified by Daniel Vadala.
* Game initiates and plays a game of war with two players each with their own cards
*/

#include <iostream>
#include <cstdio>
#include <iostream>
#include <cstdio>
#include <windows.h>
#include <GL/glew.h>
#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include <glm/glm.hpp>
#include "Camera.h"
#include "Shader.hpp"
#include "Card.h"
#include "Player.h"
#include "Table.h"
#include "ScoreKeeper.h"
#include "Light.hpp"

using namespace std;

#pragma once
class Game
{
public:
	Game();
	~Game();

	void addDrawableObject(DrawableObject*);

	void setUpGame(int, int, int, string, int, int);

	void playGame();
	void playTurn();
	void finishTurn();
	bool isGameOver();
	void addPlayers(Player*, Player*);
	void addScoreBoard(ScoreKeeper*);
	void addLight(Light*);

	void setCallback(void(*func)(sf::Window&)) {
		eventFunc = func;
	}

	
	void setCamera(Camera* c) { camera = c; }
	void setShader(Shader* s) { shader = s; }


private:



	

	Player* player1;
	Player* player2;
	vector<Card*> tiedCards;
	
	GLint numTiedCards = 0;
	bool tie = false;

	Card* currentCard1;
	Card* currentCard2;

	Table* mainTable;

	ScoreKeeper* scoreBoard;

	int player1Score, player2Score;

	int winWidth = 1000;
	int winHeight = 800;

	int tieWinner = 0;

	vec2 mousePos;

	Shader* s;
	Camera* c;
	

	vector< DrawableObject* > drawables; // The list of everything to draw
	vector< Light* > lights;
	Shader* shader; // The current shader program
	Camera* camera;
	sf::Window* window; // The OpenGL context window
	sf::Clock clock;    // The clock object
	sf::Time curTime; // The current time in the system

	void draw(); // Draw all of the drawable objects

				 // Callback functions using some function pointers
	void(*eventFunc)(sf::Window&);


	vec3 player1Deck = vec3(10, 4.2, 45);
	vec3 player2Deck = vec3(70, 4.2, 45);
	vec3 player1Discard = vec3(10, 4.2, 15);
	vec3 player2Discard = vec3(70, 4.2, 15);
	vec3 player1Play = vec3(25, 4.2, 30);
	vec3 player2Play = vec3(55, 4.2, 30);
	bool alreadyScaled = false;
	

	

};

