#version 330 core

layout(location = 0) in vec3 vPosition;

uniform mat4 PVM;

void main()
{
	gl_Position = PVM * vec4(vPosition, 1.0);
}
