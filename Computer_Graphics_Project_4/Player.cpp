/*Daniel Vadala
* 11/10/17
* Computer Graphics
* War
* Player.cpp
* Player Class holds information on a player playing the card game war.  They have a deck and a discard pile
* and a score
*/

#include "Player.h"



Player::Player(vec3 plrPoint, vec3 _startPoint, vector< Card* > cards, int playerNum, Texture* picture, int skew)
{
	//create Deck
	playerDeck = new Deck(_startPoint, cards);
	//set Player Number
	playerNumber = playerNum;
	//set Player Picture
	playerPicture = picture;
	//set Number of cards
	numCards = playerDeck->getNumCards();
	isWinner = false;
	deckPosition = _startPoint;
	center = vec3(plrPoint.x + 5, plrPoint.y + 5, plrPoint.z + (skew / 2));
	//no card vector to initialize discard pile
	vector<Card*> noCards;
	if (playerNum == 1)
	{
		discardPile = new Deck(vec3(5, 4.2, 25), noCards);
	}
	else if (playerNum == 2)
	{
		discardPile = new Deck(vec3(65, 4.2, 25), noCards);
	}
	else
	{
		cout << "A player thats not one or two? ok but you're asking for this to crash." << endl;
	}
	Vertex* v1 = new Vertex(plrPoint, vec3(1, 1, 1));
	Vertex* v2 = new Vertex(vec3(plrPoint.x + 10, plrPoint.y, plrPoint.z + skew), vec3(1, 1, 1));
	Vertex* v3 = new Vertex(vec3(plrPoint.x + 10, plrPoint.y + 10, plrPoint.z + skew), vec3(1, 1, 1));
	Vertex* v4 = new Vertex(vec3(plrPoint.x, plrPoint.y + 10, plrPoint.z), vec3(1, 1, 1));

	v1->setNormal(vec3(0, 0, 1));
	v2->setNormal(vec3(0, 0, 1));
	v3->setNormal(vec3(0, 0, 1));
	v4->setNormal(vec3(0, 0, 1));

	Triangle* t1 = new Triangle(v1, v2, v3);
	Triangle* t2 = new Triangle(v1, v3, v4);


	playerSurface = new Quad(v1, v2, v3, v4, t1, t2);
}


Player::~Player()
{
}

//shuffles players deck
void Player::shuffle()
{
	//add discard Pile to Deck
	for (int i = 0; i < discardPile->getNumCards(); i++)
	{
		playerDeck->addCard(discardPile->getCardAt(i));
		//discardPile->getCardAt(i)->moveCard(vec3(deckPosition.x, deckPosition.y + playerDeck->getDeckHeight(), deckPosition.z));
	}
	//clear discard Pile
	discardPile->clearDeck();
	//shuffle Deck
	playerDeck->shuffleDeck(deckPosition);
}

//return top card from deck
Card* Player::playCard()
{
	//temporary
	Card* playedCard = playerDeck->getCardAt(playerDeck->getNumCards() - 1);
	playerDeck->removeCard();
	numCards--;

	return playedCard;
}


void Player::discard(Card * c1)
{
	//push card into discard pile then incrament numCards
	discardPile->addCard(c1);
	numCards++;
}

//check if deck is empty
bool Player::isDeckEmpty()
{
	return playerDeck->isEmpty();
}

//check if there are discards
bool Player::isDiscardsEmpty()
{
	return discardPile->isEmpty();
}

//get height of players deck
GLfloat Player::getPlayDeckHeight()
{
	return playerDeck->getDeckHeight();
}
//get height of players discard pile
GLfloat Player::getDiscardDeckHeight()
{
	return discardPile->getDeckHeight();
}
//get players score
int Player::getScore()
{
	return numCards;
}

//draw player picture
void Player::draw(Shader* s)
{
	playerSurface->draw(s, playerPicture);
}


//scale object given a scaler vector
void Player::scale(GLfloat scaler)
{
	model = glm::scale(model, vec3(scaler, scaler, scaler));

}

//translate the object given a movement vector
void Player::translate(vec3 movement)
{
	model = glm::translate(model, movement);
}

//rotate the object given an axis of rotation and degrees of rotation
void Player::rotate(GLfloat degrees, vec3 axis)
{
	model = glm::rotate(model, degrees, axis);
}
