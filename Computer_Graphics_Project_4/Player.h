/*Daniel Vadala
* 11/10/17
* Computer Graphics
* War
* Player.h
* Player Class holds information on a player playing the card game war.  They have a deck and a discard pile
* and a score
*/

#include <iostream>
#include <cstdio>
#include <vector>
#include "DrawableObject.h"
#include "Texture.hpp"
#include "Card.h"
#include "Deck.h"
#include "Quad.h"


using namespace std;

#pragma once
class Player : public DrawableObject
{
public:
	Player(vec3, vec3, vector< Card* >, int, Texture*, int);
	~Player();

	void shuffle();
	Card* playCard();
	void discard(Card*);
	bool isDeckEmpty();
	bool isDiscardsEmpty();
	GLfloat getPlayDeckHeight();
	GLfloat getDiscardDeckHeight();
	int getScore();



	void draw(Shader*);
	void scale(GLfloat);
	void translate(vec3);
	void rotate(GLfloat, vec3);


private:
	Deck* playerDeck;//players deck
	Deck* discardPile;//players discard pile
	int playerNumber;//player number 
	Texture* playerPicture;//player representation picture
	vec3 playerPoint;//point where player picture is drawn
	Quad* playerSurface;//surface to draw player picture on
	int numCards;//number of cards the player has
	bool isWinner;

	vec3 center;

	vec3 deckPosition;
	
};

