/*Daniel Vadala and Matt Paradis
* 10/27/17
* Computer Graphics
* MineMake
* Author: Professor Stuetzle
* SpotLight.h
* SpotLight.h was created by Professor Stuetzle and modified from DirectionalLight by Daniel Vadala.  Class creates a spotlight wherever the camera looks
*/

#include "PointLight.h"

//constructor takes in parameters to create a new spotlight.  example constructor 
//spotLight = new SpotLight(vec3(0.2, 0.2, 0.8), vec3(0, 0, 0)/*c->getPosition() + vec3(-10, -10, 0)*/, 5.0, 0.51, 0.05, 0, 1, vec3(0, 0, -1), 0.984807753, 10);
PointLight::PointLight(vec3 col, vec3 pos, GLfloat st, GLfloat sh, GLfloat lnat, GLfloat quadat, GLfloat constat) :
	Light(col) {
	position = pos;//spotlight position
	strength = st;//spotlight Strength
	shininess = sh;//spotlight  Shininess

	constAttenuation = constat;//constant attenuation
	linearAttenuation = lnat;//linear attenuation
	quadraticAttenuation = quadat;//quadratic attenuation





}

PointLight::PointLight(const PointLight& orig) : Light(orig) {
}

void PointLight::moveLight(vec3 translation, Shader* s)
{
	position += translation;
	std::cout << "Position: " << position.x << ", " << position.y << ", " << position.z << std::endl;


	GLint lightPosition = s->GetVariable("LightPosition");
	s->SetVector3(lightPosition, 1, &position[0]);


}

PointLight::~PointLight() {
}

void PointLight::connectLightToShader(Shader* s) {
	// Connect variables to shaders
	GLint pntLightid = s->GetVariable("pointLightColor");
	GLint shininessid = s->GetVariable("pointShininess");
	GLint strengthid = s->GetVariable("pointStrength");
	GLint spotLightWeight = s->GetVariable("pointWeight");
	GLint lightPosition = s->GetVariable("LightPosition");
	GLint constAt = s->GetVariable("ConstantAttenuation");
	GLint linAt = s->GetVariable("LinearAttenuation");
	GLint quadAt = s->GetVariable("QuadraticAttenuation");


	s->SetVector3(pntLightid, 1, &color[0]);
	s->SetVector3(lightPosition, 1, &position[0]);
	s->SetFloat(shininessid, shininess);
	s->SetFloat(strengthid, strength);
	s->SetFloat(constAt, constAttenuation);
	s->SetFloat(linAt, linearAttenuation);
	s->SetFloat(quadAt, quadraticAttenuation);



	if (isEnabled()) {
		s->SetFloat(spotLightWeight, 1.0);
	}
	else {
		s->SetFloat(spotLightWeight, 0.0);
	}
}