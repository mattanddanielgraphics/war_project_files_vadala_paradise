#pragma once

#include "Light.hpp"
#include <glm/glm.hpp>

using glm::vec3;

class PointLight : public Light {
public:
	PointLight(vec3, vec3, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);
	PointLight(const PointLight& orig);
	void moveLight(vec3, Shader*);
	virtual ~PointLight();

	void connectLightToShader(Shader*);
private:
	vec3 position;

	GLfloat shininess;
	GLfloat strength;


	GLfloat constAttenuation;
	GLfloat linearAttenuation;
	GLfloat quadraticAttenuation;

};

