
#include "Prism.h"

/**
 This class creates a rectangular prism based off of the input.

 originX, originY, originZ - This could've been a vec3, but by separating the
	coords out it's easier to make the quads. When these three are put into a
	vec3, that will represent the back left corner of the prism.

 height - The prisms' height (y size).
 width - The prisms' width (x size).
 depth - The prisms' depth (z size).
 */
Prism::Prism(float originX, float originY, float originZ, float height, float width, float depth)
{


	v1 = new Vertex(vec3(originX, originY, originZ), vec3(1, 1, 1));
	v1->setNormal(vec3(-1, -1, -1));
	v2 = new Vertex(vec3(originX + width, originY, originZ), vec3(1, 1, 1));
	v2->setNormal(vec3(1, -1, -1));
	v3 = new Vertex(vec3(originX + width, originY, originZ + depth), vec3(1, 1, 1));
	v3->setNormal(vec3(1, -1, 1));
	v4 = new Vertex(vec3(originX, originY, originZ + depth), vec3(1, 1, 1));
	v4->setNormal(vec3(-1, -1, 1));

	v5 = new Vertex(vec3(originX + width, originY + height, originZ), vec3(1, 1, 1));
	v5->setNormal(vec3(-1, 1, -1));
	v6 = new Vertex(vec3(originX, originY + height, originZ), vec3(1, 1, 1));
	v6->setNormal(vec3(1, 1, -1));
	v7 = new Vertex(vec3(originX, originY + height, originZ + depth), vec3(1, 1, 1));
	v7->setNormal(vec3(1, 1, 1));
	v8 = new Vertex(vec3(originX + width, originY + height, originZ + depth), vec3(1, 1, 1));
	v8->setNormal(vec3(-1, 1, 1));





	t1 = new Triangle(v1, v2, v3);
	t2 = new Triangle(v1, v3, v4);
	// Bottom of prism
	q1 = new Quad(v1, v2, v3, v4, t1, t2);


	t3 = new Triangle(v1, v2, v8);
	t4 = new Triangle(v1, v8, v5);

	// Far face
	q2 = new Quad(v1, v2, v8, v5, t3, t4);

	t4 = new Triangle(v4, v3, v8);
	t5 = new Triangle(v4, v8, v7);


	// Front face
	q3 = new Quad(v4, v3, v8, v7, t4, t5);

	t7 = new Triangle(v6, v5, v8);
	t8 = new Triangle(v6, v8, v7);
	// Top face
	q4 = new Quad(v6, v5, v8, v7, t7, t8);

	t9 = new Triangle(v1, v4, v7);
	t10 = new Triangle(v1, v7, v6);
	// left face
	q5 = new Quad(v1, v4, v7, v6, t9, t10);


	t11 = new Triangle(v2, v3, v8);
	t12 = new Triangle(v2, v8, v5);
	// right face
	q6 = new Quad(v2, v3, v8, v5, t11, t12);

	quads.push_back(q1);
	quads.push_back(q2);
	quads.push_back(q3);
	quads.push_back(q4);
	quads.push_back(q5);
	quads.push_back(q6);
}

// Draws the vector of quads
void Prism::draw(Shader* s, Texture* text)
{
	for (Quad* q : quads)
		q->draw(s, text);
}

Prism::~Prism() {}



