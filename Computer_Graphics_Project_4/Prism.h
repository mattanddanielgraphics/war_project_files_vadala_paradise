#pragma once

#include <vector>
#include <glm/glm.hpp>
#include "Shader.hpp"
#include "Quad.h"
#include "DrawableObject.h"
#include "Texture.hpp"

using std::vector;
using glm::vec3;

class Prism
{
public:
	Prism(float originX, float originY, float originZ, float height, float width, float length);
	void draw(Shader*, Texture* = NULL);

	~Prism();

private:
	vec3 colorFill;
	vector<Quad*> quads;

	Vertex *v1, *v2, *v3, *v4, *v5, *v6, *v7, *v8;

	Triangle *t1, *t2, *t3, *t4, *t5, *t6, *t7, *t8, *t9, *t10, *t11, *t12;

	Quad *q1, *q2, *q3, *q4, *q5, *q6;
};

