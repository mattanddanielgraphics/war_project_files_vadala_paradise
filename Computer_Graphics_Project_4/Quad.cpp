/*Daniel Vadala and Matt Paradis
* 11/1/17
* Computer Graphics
* Project 5 MineMake
* Author: Daniel Vadala
* Quad.cpp
* Quad.cpp was created by Daniel Vadala based off the Triangle Class provided by Professor Stuetzle
*/
#include "Quad.h"


 
Quad::Quad(Vertex* _a, Vertex* _b, Vertex* _c, Vertex* _d, Triangle* t1, Triangle* t2) {
	a = _a;//bottom left
	b = _b;//bottom right
	c = _c;//top right
	d = _d;//top left

	tri1 = t1;
	tri1->calculateNormal();
	tri2 = t2;
	tri2->calculateNormal();

	//forces Z to be the same as bottom left point.  needs improvement
	verts[0] = a->getPos().x;
	verts[1] = a->getPos().y;
	verts[2] = a->getPos().z;

	verts[3] = b->getPos().x;
	verts[4] = b->getPos().y;
	verts[5] = b->getPos().z;

	verts[6] = c->getPos().x;
	verts[7] = c->getPos().y;
	verts[8] = c->getPos().z;

	verts[9] = d->getPos().x;
	verts[10] = d->getPos().y;
	verts[11] = d->getPos().z;
	// Create the texture coords
	

	tex_coords[0] = 0.0f;
	tex_coords[1] = 1.0f;
	tex_coords[2] = 0.0f;

	tex_coords[3] = 1.0f;
	tex_coords[4] = 1.0f;
	tex_coords[5] = 0.0f;

	tex_coords[6] = 1.0f;
	tex_coords[7] = 0.0f;
	tex_coords[8] = 0.0f;

	tex_coords[9] = 0.0f;
	tex_coords[10] = 0.0f;
	tex_coords[11] = 0.0f;

	normals[0] = tri1->getV1()->getNormal().x;
	normals[1] = tri1->getV1()->getNormal().y;
	normals[2] = tri1->getV1()->getNormal().z;
	normals[3] = tri1->getV2()->getNormal().x;
	normals[4] = tri1->getV2()->getNormal().y;
	normals[5] = tri1->getV2()->getNormal().z;
	normals[6] = tri1->getV3()->getNormal().x;
	normals[7] = tri1->getV3()->getNormal().y;
	normals[8] = tri1->getV3()->getNormal().z;

	normals[9] = tri2->getV1()->getNormal().x;
	normals[10] = tri2->getV1()->getNormal().y;
	normals[11] = tri2->getV1()->getNormal().z;
	normals[12] = tri2->getV2()->getNormal().x;
	normals[13] = tri2->getV2()->getNormal().y;
	normals[14] = tri2->getV2()->getNormal().z;
	normals[15] = tri2->getV3()->getNormal().x;
	normals[16] = tri2->getV3()->getNormal().y;
	normals[17] = tri2->getV3()->getNormal().z;

	

	// Set up VBO and VAO
	// Set up the VBO
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);

	// Populate with the data
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(verts) + sizeof(tex_coords) + sizeof(normals), NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(verts), &verts[0]);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(verts), sizeof(tex_coords), &tex_coords[0]);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(verts) + sizeof(tex_coords),
		sizeof(normals), &normals[0]);

	// Set up the attribute array with two attributes
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*) sizeof(verts));
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)(sizeof(verts) + sizeof(tex_coords)));
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
}


// Setters for the colors

void Quad::setFillColor(vec3 fC) {
	fillColor = fC;
}

void Quad::setBorderColor(vec3 bC) {
	borderColor = bC;
}

// The draw function

void Quad::draw(Shader* s, Texture* tex) {
	glBindVertexArray(VAO);
	s->useProgram();

	// If there's a texture, use that
	if (tex) {
		glBindTexture(GL_TEXTURE_2D, tex->getTexID());
		// Draw
		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	}
	else {
		// Make the color the fill color
		GLint c = s->GetVariable("color");
		s->SetVector3(c, 1, &fillColor[0]);
		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

		// Switch to border color for the outline
		s->SetVector3(c, 1, &borderColor[0]);
		glDrawArrays(GL_LINE_LOOP, 0, 4);
	}

	glBindVertexArray(0);
}


void Quad::update()
{
	normals[0] = tri1->getV1()->getNormal().x;
	normals[1] = tri1->getV1()->getNormal().y;
	normals[2] = tri1->getV1()->getNormal().z;
	normals[3] = tri1->getV2()->getNormal().x;
	normals[4] = tri1->getV2()->getNormal().y;
	normals[5] = tri1->getV2()->getNormal().z;
	normals[6] = tri1->getV3()->getNormal().x;
	normals[7] = tri1->getV3()->getNormal().y;
	normals[8] = tri1->getV3()->getNormal().z;

	normals[9] = tri2->getV1()->getNormal().x;
	normals[10] = tri2->getV1()->getNormal().y;
	normals[11] = tri2->getV1()->getNormal().z;
	normals[12] = tri2->getV2()->getNormal().x;
	normals[13] = tri2->getV2()->getNormal().y;
	normals[14] = tri2->getV2()->getNormal().z;
	normals[15] = tri2->getV3()->getNormal().x;
	normals[16] = tri2->getV3()->getNormal().y;
	normals[17] = tri2->getV3()->getNormal().z;



	verts[0] = a->getPos().x;
	verts[1] = a->getPos().y;
	verts[2] = a->getPos().z;

	verts[3] = b->getPos().x;
	verts[4] = b->getPos().y;
	verts[5] = b->getPos().z;

	verts[6] = c->getPos().x;
	verts[7] = c->getPos().y;
	verts[8] = c->getPos().z;

	verts[9] = d->getPos().x;
	verts[10] = d->getPos().y;
	verts[11] = d->getPos().z;

	//std::cout << d->getPos().z << std::endl;


	// Populate with the data
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(verts) + sizeof(tex_coords) + sizeof(normals), NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(verts), &verts[0]);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(verts), sizeof(tex_coords), &tex_coords[0]);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(verts) + sizeof(tex_coords),
		sizeof(normals), &normals[0]);


}




