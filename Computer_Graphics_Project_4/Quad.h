/*Daniel Vadala and Matt Paradis
* 11/1/17
* Computer Graphics
* Project 5 MineMake
* Author: Daniel Vadala
* Quad.h
* Quad.h was created by Daniel Vadala based off the Triangle Class provided by Professor Stuetzle
*/

#ifndef _QUAD_H_
#define _QUAD_H_

/*************************
*** GRAPHICS INCLUDES ***
************************/
#include <GL/glew.h>
#include <glm/vec3.hpp>
#include <glm\glm.hpp>
#include "Shader.hpp"
#include "Texture.hpp"
#include "Vertex.hpp"
#include "Triangle.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

using glm::vec3;
using glm::vec4;

class Shader;

class Quad
{
public:

	Quad(Vertex*, Vertex*, Vertex*, Vertex*, Triangle*, Triangle*);

	// The draw function
	void draw(Shader*, Texture* = NULL);

	// Set the colors
	void setFillColor(vec3);
	void setBorderColor(vec3);
	void update();





private:
	Vertex* a;
	Vertex* b;
	Vertex* c;
	Vertex* d;

	Triangle* tri1;
	Triangle* tri2;


	GLfloat normals[18];
	GLfloat tex_coords[12];
	GLfloat verts[12];
	

	// The VAO and VBO
	GLuint VAO;
	GLuint VBO;

	// Colors of the triangle
	vec3 fillColor;
	vec3 borderColor;

	
};

#endif