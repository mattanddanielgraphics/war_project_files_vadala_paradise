/*#include "Room.h"



Room::Room(vec3 roomCorner, GLfloat height, GLfloat width, GLfloat depth)
{

	//roomCorner is backLeft corner of the room
	//height is from roomCorner to ceiling
	//width is from roomcorner to right wall
	//depth is from room corner forward to the front wall

	//wall containing input room corner
	backWall = new Quad(roomCorner, vec3(roomCorner.x + width, roomCorner.y, roomCorner.z), vec3(roomCorner.x + width, roomCorner.y + height, roomCorner.z),
		vec3(roomCorner.x, roomCorner.y + height, roomCorner.z));

	frontWall = new Quad(vec3(roomCorner.x, roomCorner.y, roomCorner.z + depth), vec3(roomCorner.x + width, roomCorner.y, roomCorner.z + depth),
		vec3(roomCorner.x + width, roomCorner.y + height, roomCorner.z + depth), vec3(roomCorner.x, roomCorner.y + height, roomCorner.z + depth));

	leftWall = new Quad(roomCorner, vec3(roomCorner.x, roomCorner.y, roomCorner.z + depth), vec3(roomCorner.x, roomCorner.y + height, roomCorner.z + depth),
		vec3(roomCorner.x, roomCorner.y + height, roomCorner.z));

	rightWall = new Quad(vec3(roomCorner.x + width, roomCorner.y, roomCorner.z), vec3(roomCorner.x + width, roomCorner.y, roomCorner.z + depth),
		vec3(roomCorner.x + width, roomCorner.y + height, roomCorner.z + depth), vec3(roomCorner.x + width, roomCorner.y + height, roomCorner.z));

	floor = new Quad(roomCorner, vec3(roomCorner.x + width, roomCorner.y, roomCorner.z), vec3(roomCorner.x + width, roomCorner.y, roomCorner.z + depth),
		vec3(roomCorner.x, roomCorner.y, roomCorner.z + depth));
	roof = new Quad(vec3(roomCorner.x, roomCorner.y + height, roomCorner.z), vec3(roomCorner.x + width, roomCorner.y + height, roomCorner.z),
		vec3(roomCorner.x + width, roomCorner.y + height, roomCorner.z + depth), vec3(roomCorner.x , roomCorner.y + height, roomCorner.z + depth));

}


Room::~Room()
{
}


//draw player picture
void Room::draw(Shader* s)
{
	roof->draw(s, ceilingText);
	floor->draw(s, floorText);
	backWall->draw(s, wallText);
	leftWall->draw(s, wallText);
	rightWall->draw(s, wallText);
	frontWall->draw(s, wallText);
}


//scale object given a scaler vector
void Room::scale(GLfloat scaler)
{
	model = glm::scale(model, vec3(scaler, scaler, scaler));

}

//translate the object given a movement vector
void Room::translate(vec3 movement)
{
	model = glm::translate(model, movement);
}

//rotate the object given an axis of rotation and degrees of rotation
void Room::rotate(GLfloat degrees, vec3 axis)
{
	model = glm::rotate(model, degrees, axis);
}*/