#include "Quad.h"
#include "DrawableObject.h"

#pragma once
class Room : public DrawableObject
{
public:
	Room(vec3, GLfloat, GLfloat, GLfloat);
	~Room();
	void draw(Shader*);
	void scale(GLfloat);
	void translate(vec3);
	void rotate(GLfloat, vec3);

private:
	Quad* roof;
	Quad* floor;
	Quad* backWall;
	Quad* leftWall;
	Quad* rightWall;
	Quad* frontWall;

	Texture* wallText = new Texture("wall.png");
	Texture* ceilingText = new Texture("ceiling.png");
	Texture* floorText = new Texture("carpet.png");


};

