/*Daniel Vadala
* 10/27/17
* Computer Graphics
* Models in Space
* Author: Professor Stuetzle
* SFMLApplication.cpp
* SFMLApplication.cpp was created by Professor Stuetzle and modified by Daniel Vadala.  Class creates a window and draws a vector of drawable objects to the scene with a camera to view them
*/

#include "SFMLApplication.h"

SFMLApplication::SFMLApplication() {

}

SFMLApplication::SFMLApplication(const SFMLApplication& orig) {

}

SFMLApplication::~SFMLApplication() {
}

void SFMLApplication::initializeApplication(int aaValue, // anti-aliasing level 
        int minorVersion, // OpenGL version
        int majorVersion,
        string winTitle, // The title of the window
        int winWidth, // Width and height of the window
        int winHeight
        ) {

	

    // Create the window
    sf::ContextSettings settings;
	settings.depthBits = 24;
    settings.antialiasingLevel = aaValue;
    // If you are getting a failure, comment out these lines first:
    settings.majorVersion = majorVersion;
    settings.minorVersion = minorVersion;


    // To be safe, we’re using glew’s experimental stuff.
    glewExperimental = GL_TRUE;
    // Initialize and error check GLEW
    GLenum err = glewInit();
    if (GLEW_OK != err) {
        // If something went wrong, print the error message
        fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
    }

	

	//enable tests
    glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_NORMALIZE);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT1);

    glClearDepth(1.0f);
    
    // Set the time
    clock.restart();
    curTime = clock.getElapsedTime();
}



void SFMLApplication::draw(sf::Window* window) {    

	//craeate directional light, above and beyond attempt
	GLfloat lightColor[] = { 0.5f, 0.5f, 0.5f, 1.0f };
	GLfloat lightPosition[] = { -1.0f, 0.5f, 0.5f, 0.0f};
	glLightfv(GL_LIGHT1, GL_DIFFUSE, lightColor);
	glLightfv(GL_LIGHT1, GL_POSITION, lightPosition);

	Texture* cardTexture = new Texture("Cards/2_of_clubs.png");

    // Now, set the "delta time"
    double dt = clock.getElapsedTime().asSeconds() - curTime.asSeconds();
    curTime =  clock.getElapsedTime();
    camera -> setDeltaTime(dt);
    
    // Clears the given framebuffer (in this case, color and depth)
    // Could set color to clear to with glClearColor, default is black
    // Also clears the depth buffer, IMPORTANT because we're now in 3D
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Update the camera
    mat4 projectionMatrix = camera -> getPerspectiveMatrix();
    mat4 viewMatrix = camera -> getViewMatrix();

    // Draw everything
    for (int i = 0; i < drawables.size(); i++) {
        if (drawables[i]) {
            mat4 modelMatrix = drawables[i] -> getModelMatrix();
            mat4 PVMMatrix = projectionMatrix * viewMatrix * modelMatrix;
            GLint PVMid = shader -> GetVariable("PVM");
            shader -> SetMatrix4(PVMid, 1, false, &PVMMatrix[0][0]);

            drawables[i] -> draw(shader, NULL);
        }
    }

    // Force OpenGL commands to begin execution
    window -> display();
}



// Add a drawable object to the class

void SFMLApplication::addDrawableObject(DrawableObject* o) {
    drawables.push_back(o);
}
//returns object given a vector position
DrawableObject * SFMLApplication::getObject(int current)
{
	return drawables[current];
}
