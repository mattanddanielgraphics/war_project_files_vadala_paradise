/*Daniel Vadala
* 10/27/17
* Computer Graphics
* Models in Space
* Author: Professor Stuetzle
* SFMLApplication.h
* SFMLApplication.h was created by Professor Stuetzle and modified by Daniel Vadala.  Class creates a window and draws a vector of drawable objects to the scene with a camera to view them
*/

#ifndef SFMLAPPLICATION_H
#define SFMLAPPLICATION_H

#include <GL/glew.h>
#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include <glm/glm.hpp>
#include <vector>
#include "Shader.hpp"
#include "Camera.h"


#include "DrawableObject.h"

using glm::vec3;
using glm::vec2;
using glm::mat4;
using std::vector;

class SFMLApplication {
public:
    SFMLApplication();
    SFMLApplication(const SFMLApplication& orig);
    virtual ~SFMLApplication();

    // General Initialization Function
    void initializeApplication(int, int, int, string, int, int);

    // Add a drawable object to the list
    void addDrawableObject(DrawableObject*);
    void setShaderProgram(Shader*); // Set the current shader program
	DrawableObject* getObject(int);

    // Set the function to handle the events
    void setCallback( void (*func)(sf::Window&)) {
        eventFunc = func;
    }
    
    // Set the camera and the shader
    void setCamera(Camera* c) { camera = c; }
    void setShader(Shader* s) { shader = s; }

	void draw(sf::Window*); // Draw all of the drawable objects
	void(*eventFunc)(sf::Window&);

	//sf::Window* getWindow();

private:
    vector< DrawableObject* > drawables; // The list of everything to draw
    Shader* shader; // The current shader program
    Camera* camera;
    //sf::Window* window; // The OpenGL context window
    sf::Clock clock;    // The clock object
    sf::Time curTime; // The current time in the system

    

    // Callback functions using some function pointers
    

	
    
};

#endif /* GLFWAPPLICATION_H */

