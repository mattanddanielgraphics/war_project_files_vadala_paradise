/*Matt Paradis
  Scorekeeper.cpp
  Class that makes a scoreboard to be used in the game of war to show
  each players current score*/

#include "ScoreKeeper.h"

/*Constructor for the scoreboard takes in the starting point for where to build the scoreboard*/
ScoreKeeper::ScoreKeeper(vec3 _startPoint) {
	startPoint = _startPoint;

	//make the depth to draw scoreboard the depth of the starting draw point's z
	scoreBoardDepth = startPoint.z;

	createScoreboardPoints();
}

/*Functions to set players scores*/
void ScoreKeeper::setPlayer1Score(int s) {
	player1Score = s;
	determineScoreTextures();
}
void ScoreKeeper::setPlayer2Score(int s) {
	player2Score = s;
	determineScoreTextures();
}

/*Function to determine which textures to use to represent each players score*/
void ScoreKeeper::determineScoreTextures() {
	int texIndex;	//index of the number texture to use

	//mod player score by 10 to get the 1s place
	texIndex = player1Score % 10;
	player1OnesTex = numberTextures[texIndex];
	//divide and mod by 10 to get the tens place (may not need to mod by 10)
	texIndex = (int)(player1Score / 10) % 10;
	player1TensTex = numberTextures[texIndex];

	//mod player score by 10 to get the 1s place
	texIndex = player2Score % 10;
	player2OnesTex = numberTextures[texIndex];
	//divide and mod by 10 to get the tens place (may not need to mod by 10)
	texIndex = (int)(player2Score / 10) % 10;
	player2TensTex = numberTextures[texIndex];
}

/*Creates the points for where to build player 1 and player 2's scoreboards based
  on the starting point that gets passed in when creating the scoreboard*/
void ScoreKeeper::createScoreboardPoints() {
	//Points for player1s scoreboard
	vec3 p1, p2, p3, p4, p5, p6, p7, p8;

	//create the points for player 1
	p1 = startPoint;
	p2.x = p1.x + namePlateWidth;  p2.y = p1.y;					   p2.z = scoreBoardDepth;
	p3.x = p1.x;				   p3.y = p1.y - scoreBoardHeight; p3.z = scoreBoardDepth;
	p4.x = p2.x;				   p4.y = p3.y;					   p4.z = scoreBoardDepth;
	p5.x = p2.x + scorePlateWidth; p5.y = p1.y;					   p5.z = scoreBoardDepth;
	p6.x = p5.x;				   p6.y = p1.y - scoreBoardHeight; p6.z = scoreBoardDepth;
	p7.x = p5.x + scorePlateWidth; p7.y = p1.y;				       p7.z = scoreBoardDepth;
	p8.x = p7.x;				   p8.y = p6.y;					   p8.z = scoreBoardDepth;

	v1 = new Vertex(p1, vec3(1,1,1));
	v2 = new Vertex(p2, vec3(1, 1, 1));
	v3 = new Vertex(p3, vec3(1, 1, 1));
	v4 = new Vertex(p4, vec3(1, 1, 1));

	v5 = new Vertex(p5, vec3(1, 1, 1));
	v6 = new Vertex(p6, vec3(1, 1, 1));
	v7 = new Vertex(p7, vec3(1, 1, 1));
	v8 = new Vertex(p8, vec3(1, 1, 1));

	v1->setNormal(vec3(0, 0, 1));
	v2->setNormal(vec3(0, 0, 1));
	v3->setNormal(vec3(0, 0, 1));
	v4->setNormal(vec3(0, 0, 1));
	v5->setNormal(vec3(0, 0, 1));
	v6->setNormal(vec3(0, 0, 1));
	v7->setNormal(vec3(0, 0, 1));
	v8->setNormal(vec3(0, 0, 1));

	t1 = new Triangle(v3, v4, v2);
	t2 = new Triangle(v3, v2, v1);

	t3 = new Triangle(v4, v6, v5);
	t4 = new Triangle(v4, v5, v2);

	t5 = new Triangle(v6, v8, v7);
	t6 = new Triangle(v6, v7, v5);


	//create player1's scoreboard squares
	player1Plate     = new Quad(v3, v4, v2, v1, t1, t2);
	player1TensPlace = new Quad(v4, v6, v5, v2, t3, t4);
	player1OnesPlace = new Quad(v6, v8, v7, v5, t5, t6);

	//create points for player 2 start by shifting over the starting point and build from there
	startPoint.x = startPoint.x + gap;
	p1 = startPoint;
	p2.x = p1.x + namePlateWidth;  p2.y = p1.y;					   p2.z = scoreBoardDepth;
	p3.x = p1.x;				   p3.y = p1.y - scoreBoardHeight; p3.z = scoreBoardDepth;
	p4.x = p2.x;				   p4.y = p3.y;					   p4.z = scoreBoardDepth;
	p5.x = p2.x + scorePlateWidth; p5.y = p1.y;					   p5.z = scoreBoardDepth;
	p6.x = p5.x;				   p6.y = p1.y - scoreBoardHeight; p6.z = scoreBoardDepth;
	p7.x = p5.x + scorePlateWidth; p7.y = p1.y;				       p7.z = scoreBoardDepth;
	p8.x = p7.x;				   p8.y = p6.y;					   p8.z = scoreBoardDepth;

	v9 = new Vertex(p1, vec3(1, 1, 1));
	v10 = new Vertex(p2, vec3(1, 1, 1));
	v11 = new Vertex(p3, vec3(1, 1, 1));
	v12 = new Vertex(p4, vec3(1, 1, 1));

	v13 = new Vertex(p5, vec3(1, 1, 1));
	v14 = new Vertex(p6, vec3(1, 1, 1));
	v15 = new Vertex(p7, vec3(1, 1, 1));
	v16 = new Vertex(p8, vec3(1, 1, 1));

	v9->setNormal(vec3(0, 0, 1));
	v10->setNormal(vec3(0, 0, 1));
	v11->setNormal(vec3(0, 0, 1));
	v12->setNormal(vec3(0, 0, 1));
	v13->setNormal(vec3(0, 0, 1));
	v14->setNormal(vec3(0, 0, 1));
	v15->setNormal(vec3(0, 0, 1));
	v16->setNormal(vec3(0, 0, 1));

	t7 = new Triangle(v11, v12, v10);
	t8 = new Triangle(v11, v10, v9);

	t9 = new Triangle(v12, v14, v13);
	t10 = new Triangle(v12, v13, v10);

	t11 = new Triangle(v14, v16, v15);
	t12 = new Triangle(v14, v15, v13);

	//create player2s scoreboard squares
	player2Plate     = new Quad(v11, v12, v10, v9, t7, t8);
	player2TensPlace = new Quad(v12, v14, v13, v10, t9, t10);
	player2OnesPlace = new Quad(v14, v16, v15, v13, t11, t12);
}

//the draw function
void ScoreKeeper::draw(Shader* s) {
	//update the textures to use
	determineScoreTextures();

	player1Plate->draw(s, player1PlateTex);
	player1TensPlace->draw(s, player1TensTex);
	player1OnesPlace->draw(s, player1OnesTex);

	player2Plate->draw(s, player2PlateTex);
	player2TensPlace->draw(s, player2TensTex);
	player2OnesPlace->draw(s, player2OnesTex);
}

ScoreKeeper::~ScoreKeeper() {

}

//scale object given a scaler vector
void ScoreKeeper::scale(GLfloat scaler)
{
	model = glm::scale(model, vec3(scaler, scaler, scaler));

}

//translate the object given a movement vector
void ScoreKeeper::translate(vec3 movement)
{
	model = glm::translate(model, movement);
}

//rotate the object given an axis of rotation and degrees of rotation
void ScoreKeeper::rotate(GLfloat degrees, vec3 axis)
{
	model = glm::rotate(model, degrees, axis);
}