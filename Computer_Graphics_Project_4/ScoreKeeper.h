/*Matt Paradis
  ScoreKeeper.h
Class that visualizes the socre of player 1 and 2 to the 
screen*/

#include <string>
#include <glm/gtc/matrix_transform.hpp>
#include "DrawableObject.h"
#include "Shader.hpp"
#include "Quad.h"
#include "Texture.hpp"

using namespace std;
using glm::vec3;

class ScoreKeeper : public DrawableObject
{
public:
	ScoreKeeper(vec3);
	~ScoreKeeper();

	void setPlayer1Score(int);
	void setPlayer2Score(int);

	void scale(GLfloat);
	void translate(vec3);
	void rotate(GLfloat, vec3);

	void draw(Shader*);

private:
	//helper functions for making the textures and points
	void determineScoreTextures();
	void createScoreboardPoints();

	//each player's starting score
	int player1Score = 12;
	int player2Score = 34;

	//ints for the dimensions of the players scoreboard
	int namePlateWidth = 15;
	int scorePlateWidth = 5;
	int scoreBoardHeight = 5;
	int scoreBoardDepth = 5;	//how far back to make the scoreboard
	int gap = 55;				//gap between  two players scores

	//where to start building the points from 
	vec3 startPoint;

	//squares for the player's name plates
	Quad* player1Plate;
	Quad* player2Plate;
	
	Quad* player1OnesPlace;
	Quad* player1TensPlace;
	Quad* player2OnesPlace;
	Quad* player2TensPlace;

	Vertex *v1, *v2, *v3, *v4, *v5, *v6, *v7, *v8;
	Vertex *v9, *v10, *v11, *v12, *v13, *v14, *v15, *v16;
	Triangle *t1, *t2, *t3, *t4, *t5, *t6, *t7, *t8, *t9, *t10, *t11, *t12;

	Texture* player1PlateTex = new Texture("scoreTextures/player1.png");
	Texture* player2PlateTex = new Texture("scoreTextures/player2.png");

	Texture* player1OnesTex;
	Texture* player1TensTex;
	Texture* player2OnesTex;
	Texture* player2TensTex;


	Texture* numberTextures[10] = { new Texture("scoreTextures/zero.png"), new Texture("scoreTextures/one.png"),
		new Texture("scoreTextures/two.png"),  new Texture("scoreTextures/three.png"),
		new Texture("scoreTextures/four.png"), new Texture("scoreTextures/five.png"),
		new Texture("scoreTextures/six.png"),  new Texture("scoreTextures/seven.png"),
		new Texture("scoreTextures/eight.png"),new Texture("scoreTextures/nine.png") };

};