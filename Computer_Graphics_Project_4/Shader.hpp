#ifndef _SHADER_PROGRAM_H_
#define _SHADER_PROGRAM_H_

#define GLEW_STATIC

/**************************
Code from chris stuetzle
**** GRAPHICS INCLUDES ***
*************************/

#include <GL\glew.h>
#include <string>
#include <cstdlib>
#include <iostream>
#include <stdio.h>
#include <glm/glm.hpp>

using std::string;
using std::cerr;
using std::endl;
using glm::mat2;

#define BUFFER_OFFSET(i) ((void*)(intptr_t)(i))

typedef struct {
	GLenum type;
	const char* filename;
	GLuint shader;
} ShaderInfo;

class Shader {
private:
	GLuint ShaderProgramId;
	void initialize(string[]);
	GLuint LoadShaders(ShaderInfo*);
	const GLchar* ReadShader(const char*);

public:
	// Turn this shader off and on
	void useProgram();
	void stopUsingProgram();

	// Interact with the shaders via GLSL and OpenGL API
	GLint GetVariable(string);
	void SetMatrix4(GLint, GLsizei, GLboolean, const GLfloat*);
	void SetMatrix3(GLint, GLsizei, GLboolean, const GLfloat*);
	void SetVector3(GLint, GLsizei, const GLfloat*);
	void SetFloat(GLint id, float value);

	Shader(string[], bool);
};




#endif


/* Parts of this class were copied from gametutorials.com.


*/