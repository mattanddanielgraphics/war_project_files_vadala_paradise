/*Daniel Vadala and Matt Paradis
* 10/27/17
* Computer Graphics
* MineMake
* Author: Professor Stuetzle
* SkyBox.h
* SkyBox.h was created by Professor Stuetzle and modified from Cube by Daniel Vadala.  Class creates a box around the world with textured sides
*/

#include "SkyBox.h"

SkyBox::SkyBox() {
	assert(false && "Do not use default cube constructor.");
}

// Constructor with four corners and colors

SkyBox::SkyBox(vec3 botLeft, double _size, vec3 fC, vec3 bC) {
	fillColor = fC;
	borderColor = bC;
	size = _size;

	//bBoxMax = botLeft;
	//bBoxMin = botLeft + vec3(size, size, size);

	


	vertices[0] = new Vertex(botLeft, fC);
	vertices[0]->setNormal(glm::normalize(vec3(1, 1, 1)));
	vertices[1] = new Vertex(botLeft + vec3(size, 0.0, 0.0), fC);
	vertices[1]->setNormal(glm::normalize(vec3(-1, 1, 1)));
	vertices[2] = new Vertex(botLeft + vec3(size, size, 0.0), fC);
	vertices[2]->setNormal(glm::normalize(vec3(-1, -1, 1)));
	vertices[3] = new Vertex(botLeft + vec3(0.0, size, 0.0), fC);
	vertices[3]->setNormal(glm::normalize(vec3(1, -1, 1)));
	vertices[4] = new Vertex(botLeft + vec3(0.0, 0.0, size), fC);
	vertices[4]->setNormal(glm::normalize(vec3(1, 1, -1)));
	vertices[5] = new Vertex(botLeft + vec3(size, 0.0, size), fC);
	vertices[5]->setNormal(glm::normalize(vec3(-1, 1, -1)));
	vertices[6] = new Vertex(botLeft + vec3(size, size, size), fC);
	vertices[6]->setNormal(glm::normalize(vec3(-1, -1, -1)));
	vertices[7] = new Vertex(botLeft + vec3(0.0, size, size), fC);
	vertices[7]->setNormal(glm::normalize(vec3(1, -1, -1)));

	// Set up the triangles
	triangles[0] = new Triangle(vertices[0], vertices[1], vertices[2]);
	vertices[0]->addTriangle(triangles[0]);
	vertices[1]->addTriangle(triangles[0]);
	vertices[2]->addTriangle(triangles[0]);
	triangles[1] = new Triangle(vertices[0], vertices[2], vertices[3]);
	vertices[0]->addTriangle(triangles[1]);
	vertices[2]->addTriangle(triangles[1]);
	vertices[3]->addTriangle(triangles[1]);
	triangles[2] = new Triangle(vertices[0], vertices[5], vertices[1]);
	vertices[0]->addTriangle(triangles[2]);
	vertices[5]->addTriangle(triangles[2]);
	vertices[1]->addTriangle(triangles[2]);
	triangles[3] = new Triangle(vertices[0], vertices[4], vertices[5]);
	vertices[0]->addTriangle(triangles[3]);
	vertices[4]->addTriangle(triangles[3]);
	vertices[5]->addTriangle(triangles[3]);
	triangles[4] = new Triangle(vertices[0], vertices[3], vertices[4]);
	vertices[0]->addTriangle(triangles[4]);
	vertices[3]->addTriangle(triangles[4]);
	vertices[4]->addTriangle(triangles[4]);
	triangles[5] = new Triangle(vertices[7], vertices[4], vertices[3]);
	vertices[7]->addTriangle(triangles[5]);
	vertices[4]->addTriangle(triangles[5]);
	vertices[3]->addTriangle(triangles[5]);
	triangles[6] = new Triangle(vertices[1], vertices[6], vertices[2]);
	vertices[1]->addTriangle(triangles[6]);
	vertices[6]->addTriangle(triangles[6]);
	vertices[2]->addTriangle(triangles[6]);
	triangles[7] = new Triangle(vertices[1], vertices[5], vertices[6]);
	vertices[1]->addTriangle(triangles[7]);
	vertices[5]->addTriangle(triangles[7]);
	vertices[6]->addTriangle(triangles[7]);
	triangles[8] = new Triangle(vertices[2], vertices[6], vertices[3]);
	vertices[2]->addTriangle(triangles[8]);
	vertices[6]->addTriangle(triangles[8]);
	vertices[3]->addTriangle(triangles[8]);
	triangles[9] = new Triangle(vertices[3], vertices[6], vertices[7]);
	vertices[3]->addTriangle(triangles[9]);
	vertices[6]->addTriangle(triangles[9]);
	vertices[7]->addTriangle(triangles[9]);
	triangles[10] = new Triangle(vertices[7], vertices[6], vertices[4]);
	vertices[7]->addTriangle(triangles[10]);
	vertices[6]->addTriangle(triangles[10]);
	vertices[4]->addTriangle(triangles[10]);
	triangles[11] = new Triangle(vertices[4], vertices[6], vertices[5]);
	vertices[4]->addTriangle(triangles[11]);
	vertices[6]->addTriangle(triangles[11]);
	vertices[5]->addTriangle(triangles[11]);

	for (int i = 0; i < 12; i++) {
		triangles[i]->calculateNormal();
	}

	front = new Quad(vertices[0], vertices[1], vertices[2], vertices[3], triangles[0], triangles[1]);
	back = new Quad(vertices[4], vertices[5], vertices[6], vertices[7], triangles[10], triangles[11]);
	top = new Quad(vertices[3], vertices[2], vertices[6], vertices[7], triangles[8], triangles[9]);
	bottom = new Quad(vertices[0], vertices[1], vertices[5], vertices[4], triangles[2], triangles[3]);
	right = new Quad(vertices[1], vertices[5], vertices[6], vertices[2], triangles[6], triangles[7]);
	left = new Quad(vertices[0], vertices[4], vertices[7], vertices[3], triangles[4], triangles[5]);


	
}

// Copy constructor is UNDER CONSTRUCTION (get it?!)


SkyBox::~SkyBox() {
	// Delete triangles

	// Delete vertices
}

// The drawing function
// Pre: None
// Post: The tetrahedron is drawn to the screen

void SkyBox::draw(Shader* s) {

	front->draw(s, wallText);
	back->draw(s, wallText);
	top->draw(s, skyTexture);
	bottom->draw(s, underGroundTexture);
	right->draw(s, wallText);
	left->draw(s, wallText);
}

/*
vec3 Cube::getEyeDirection( Camera* c, int vertNum ) {
assert( vertNum < 8 && vertNum >= 0 );
return glm::normalize(c->getPosition() - vertices[vertNum]->getPos());
}
* */

//scale object given a scaler vector
void SkyBox::scale(GLfloat scaler)
{
	model = glm::scale(model, vec3(scaler, scaler, scaler));

}

//translate the object given a movement vector
void SkyBox::translate(vec3 movement)
{
	applyTransformation(glm::translate(glm::mat4(1.0), movement));
	//model = glm::translate(model, movement);
	
	
	for (int i = 0; i < 8; i++)
	{
		vertices[i]->translatePosition(movement);
	}

	for (int i = 0; i < 12; i++) {
		triangles[i]->calculateNormal();
	}

	front->update();
	back->update();
	top->update();
	bottom->update();
	right->update();
	left->update();
}

//rotate the object given an axis of rotation and degrees of rotation
void SkyBox::rotate(GLfloat degrees, vec3 axis)
{
	model = glm::rotate(model, degrees, axis);
}

void SkyBox::setTextures(Texture * wall, Texture * top, Texture * bottom)
{
	wallText = wall;
	skyTexture = top;
	underGroundTexture = bottom;
}

