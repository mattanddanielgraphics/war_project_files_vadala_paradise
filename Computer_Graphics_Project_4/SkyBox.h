/*Daniel Vadala and Matt Paradis
* 10/27/17
* Computer Graphics
* MineMake
* Author: Professor Stuetzle
* SkyBox.h
* SkyBox.h was created by Professor Stuetzle and modified from Cube by Daniel Vadala.  Class creates a box around the world with textured sides
*/

#ifndef SKYBOX_H
#define SKYBOX_H

#include "Triangle.hpp"
#include "Shader.hpp"
#include "DrawableObject.h"
#include "Vertex.hpp"
#include "Texture.hpp"
#include "Quad.h"
#include <GL/glew.h>

using glm::vec3;
class Triangle;

class SkyBox : public DrawableObject {
public:
	SkyBox();
	SkyBox(vec3, double,
		vec3 fillColor = vec3(1.0f, 1.0f, 1.0f),
		vec3 borderColor = vec3(0.0f, 0.0f, 0.0f));
	virtual ~SkyBox();

	void scale(GLfloat);
	void translate(vec3);
	void rotate(GLfloat, vec3);

	void setTextures(Texture*, Texture*, Texture*);

	// The draw function
	void draw(Shader*);


	//vec3 getEyeDirection(Camera*, int vertNum = 0 );
private:
	Triangle* triangles[12];
	Vertex* vertices[8];
	double size;


	Quad* front;
	Quad* back;
	Quad* bottom;
	Quad* top;
	Quad* left;
	Quad* right;


	// Colors
	vec3 fillColor;
	vec3 borderColor;

	Texture* wallText;
	Texture* skyTexture;
	Texture* underGroundTexture;


	// Set up the VBO and VAO
	GLuint VAO;
	GLuint VBO;
};

#endif /* SKYBOX_H */

