/* Matt Paradis
   Code that will generate a square and draw it into the world
   ability to use textures
*/
#include "Square.h"

Square::Square() {
}

/*This constructor takes in all 4 points for where to build the square*/
Square::Square(vec3 _a, vec3 _b, vec3 _c, vec3 _d) {
	a = _a;
	b = _b;
	c = _c;
	d = _d;

	setup();
}

/*This constructor takes in the top left and the bottom right 
points of the suqare and will fill in the two missing points 
to complete the square
*/
Square::Square(vec3 _a, vec3 _c) {

	vec3 _b =  { _c.x, _a.y, _a.z };
	vec3 _d  = { _a.x, _c.y, _c.z };

	a = _a;	//top left point
	b = _b;	
	c = _c;
	d = _d;

	setup();
}

vec3 Square::getCorner() {
	return a;
}

void Square::movePoints(vec3 _a, vec3 _c) {
	vec3 _b = { _c.x, _a.y, _a.z };
	vec3 _d = { _a.x, _c.y, _c.z };

	a = _a;	//top left point
	b = _b;
	c = _c;
	d = _d;

	setup();
}

/*Sets up the verts arrays and the buffers  so that it can be drawn*/
void Square::setup() {
	GLfloat verts[] = { a.x, a.y, a.z,
		b.x, b.y, b.z,
		c.x, c.y, c.z,
		d.x, d.y, d.z };

	GLuint elements[] = {
		0, 1, 2,
		2, 3, 0 };

	GLfloat tex_coords[] = { 0.0f,  1.0f,
		1.0f,  1.0f,
		1.0f,  0.0f,
		0.0f,  0.0f };

	// Set up the VBO
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);
	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);

	//Populate with the date
	glBufferData(GL_ARRAY_BUFFER, sizeof(verts) + sizeof(tex_coords), verts, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(verts), verts);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(verts), sizeof(tex_coords), tex_coords);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
		sizeof(elements), elements, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*) sizeof(verts));
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	// By default the color is "red"
	fillColor = vec3(1.0f, 0.0f, 0.0f);
}

//set the  fill color of the square
void Square::setFillColor(vec3 fCol) {
	fillColor = fCol;
}
void Square::setBorderColor(vec3 bCol) {
	borderColor = bCol;
}

//sets the width of the square
void Square::setLineWidth(int width) {
	lineWidth = width;
}

//determines if the user wants to fill the square with color
void Square::shouldFill(bool tf) {
	fill = tf;
}

/*Function to take in the mousex and mouseY coords and determine
  if the mouse was pressed inside of this square if so returns TRUE*/
bool Square::mouseInBounds(double mouseX, double mouseY) {
	if ((mouseX > a.x) && (mouseX < c.x)) {
		if ((mouseY < a.y) && (mouseY > c.y)) {
			return true;
		}
	}
	return false;
}

//draw the square
void Square::draw(Shader* s, Texture* tex) {

	glLineWidth(lineWidth);
	glBindVertexArray(VAO);

	s->useProgram();

	//if there is a texture use it 
	if (tex) {
		glBindTexture(GL_TEXTURE_2D, tex->getTexID());
		//Draw
		glDrawElements(GL_TRIANGLE_FAN, 6, GL_UNSIGNED_INT, 0);
	}
	else {
		GLint c = s->GetVariable("color");
		s->SetVector3(c, 1, &fillColor[0]);
		glDrawElements(GL_TRIANGLE_FAN, 6, GL_UNSIGNED_INT, 0);
	}
	// Switch to border color for the outline
	GLint c = s->GetVariable("color");
	s->SetVector3(c, 1, &borderColor[0]);
	glDrawElements(GL_TRIANGLE_FAN, 6, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}