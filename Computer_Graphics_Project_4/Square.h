/*
Matt Paradis
class that builds a square and draws it into the world
able to use textures
*/

#ifndef _SQUARE_H_
#define _SQUARE_H_

#include <GL/glew.h>
#include <glm/vec3.hpp>
#include "Shader.hpp"
#include "Texture.hpp"

using glm::vec3;
using glm::vec4;

class Shader;

class Square 
{
public:
	Square();
	Square(vec3 _a, vec3 _b, vec3 _c, vec3 _d);
	Square(vec3 _a, vec3 _b);

	vec3 getCorner();
	void movePoints(vec3 _a, vec3 _c);
	void setup();
	void setLineWidth(int width);
	void shouldFill(bool tf);
	bool mouseInBounds(double mouseX, double mouseY);

	//Set the colors
	void setFillColor(vec3 newCol);
	void setBorderColor(vec3);

	//The draw function
	void draw(Shader* s, Texture* = NULL);

private:
	//the four corners
	vec3 a;
	vec3 b;
	vec3 c;
	vec3 d;

	//The VAO, VBO and EBO
	GLuint VAO;
	GLuint VBO;
	GLuint EBO;

	//The color of the square
	vec3 fillColor = vec3(0.0f, 0.0f, 0.0f);
	vec3 borderColor = vec3(0.0f, 0.0f, 0.0f);

	int lineWidth = 6;
	bool fill = true;
};
#endif
