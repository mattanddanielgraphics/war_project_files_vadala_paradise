
#include "Table.h"

using glm::mat4;

/**
Alright boys, here it is.

origin - The origin point of the table. It's the xCoord of the back right
corner.
thickness - The thickness of the table.
width - The width (x size) of the table.
depth - The depth (z size) of the table.
legHeight - The height of the table's legs.
legSize - The size of the table's legs.
*/
Table::Table(float origin, float thickness, float width, float depth, float legHeight, float legSize)
{
	mat4 matrixIdentity = mat4(1.0f);
	vec3 matrixScale = vec3(1.25, 0, 1.25);

	base = new Prism(origin, origin, origin, thickness, width, depth);
	
	float legDisplacement = .75f;
	float legActualHeight = -thickness - legHeight;

	leg1 = new Prism(origin + legDisplacement, origin, origin, legActualHeight, legSize, legSize);
	leg2 = new Prism(origin + legDisplacement, origin, width - depth, legActualHeight, legSize, legSize);
	leg3 = new Prism(origin + width - (3 * legDisplacement), origin, origin, legActualHeight, legSize, legSize);
	leg4 = new Prism(origin + width - (3 * legDisplacement), origin, width - depth, legActualHeight, legSize, legSize);

	prisms.push_back(base);
	prisms.push_back(leg1);
	prisms.push_back(leg2);
	prisms.push_back(leg3);
	prisms.push_back(leg4);
}

void Table::draw(Shader* s)
{
	for (Prism* p : prisms)
		p->draw(s, text);
}

Table::~Table() {}


//scale object given a scaler vector
void Table::scale(GLfloat scaler)
{
	model = glm::scale(model, vec3(scaler, scaler, scaler));

}

//translate the object given a movement vector
void Table::translate(vec3 movement)
{
	model = glm::translate(model, movement);
}

//rotate the object given an axis of rotation and degrees of rotation
void Table::rotate(GLfloat degrees, vec3 axis)
{
	model = glm::rotate(model, degrees, axis);
}


//select the object
void Table::select(bool status)
{
	isSelected = status;
}

