#pragma once

#include <vector>
#include <glm/glm.hpp>
#include "Shader.hpp"
#include "Prism.h"
#include "DrawableObject.h"

using std::vector;
using glm::vec3;
using glm::mat4;

class Table : public DrawableObject
{
public:
	Table(float origin, float height, float width, float depth, float tableHeight, float legSize);
	void draw(Shader* shader);


	void scale(GLfloat);
	void translate(vec3);
	void rotate(GLfloat, vec3);
	void select(bool);

	~Table();

private:
	vector<Prism*> prisms;
	Prism *base, *leg1, *leg2, *leg3, *leg4;

	Texture* text = new Texture("wood.png");
	

};
