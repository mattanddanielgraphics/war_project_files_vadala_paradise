#version 330 core



uniform sampler2D tex;

// These are between 0.0 and 1.0, so they must be scaled to the image size
in vec2 vs_tex_coord;

out vec4 fColor;

void main(void)
{
  fColor = texture(tex, vs_tex_coord);
}
