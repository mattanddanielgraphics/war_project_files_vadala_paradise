#version 330 core

layout (location = 0) in vec3 vPosition;
layout (location = 1) in vec2 in_tex_coord;

uniform mat4 PVM;

out vec2 vs_tex_coord;

void main(void)
{
	gl_Position = PVM * vec4(vPosition, 1.0);
  	vs_tex_coord = in_tex_coord;
}
