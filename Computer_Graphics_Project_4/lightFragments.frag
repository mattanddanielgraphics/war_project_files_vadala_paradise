#version 330 core

// Variables coming in from the vertex shader
//in vec2 vs_tex_coord;
//in vec3 Color;
//in vec3 Normal;
//in vec4 Position;
//in vec3 dirHalfVector;
//in vec3 spotHalfVector;

in vec2 vs_tex_coord;
in vec3 Normal;
in vec3 pointHalfVector;
in vec4 Position;

out vec4 FragColor;

uniform vec3 ambientColor;
uniform float ambientWeight;
uniform vec3 pointLightColor;
uniform float pointWeight;
uniform vec3 LightPosition;
uniform float pointShininess;
uniform float pointStrength;
uniform float ConstantAttenuation;
uniform float LinearAttenuation;
uniform float QuadraticAttenuation;

uniform sampler2D tex;

void main() 
{
	// compute cosine of the directions, using dot products,
	// to see how much light would be reflected

	vec4 surfaceColor = texture(tex, vs_tex_coord);


	vec3 pointLightDirection = LightPosition - vec3(Position);
	float lightDistance = length(pointLightDirection);
	pointLightDirection = pointLightDirection / lightDistance;
	
	
	float attenuation = 1.0 / (ConstantAttenuation +
	LinearAttenuation * lightDistance +
	QuadraticAttenuation * lightDistance * lightDistance);

	// how close are we to being in the spot?
	
	//calculate Diffuse
	float pointDiffuse = max(0.0,dot(Normal, pointLightDirection));
	float pointSpecular = max(0.0, dot(Normal, pointHalfVector));



	// surfaces facing away from the light (negative dot products)
	// wont be lit by the directional light

	if (pointDiffuse == 0.0)
		pointSpecular = 0.0;
	else
		pointSpecular = pow(pointSpecular, pointShininess);
		
	//calculate scatteredLight
	vec3 ScatteredLight = normalize((ambientWeight * ambientColor) + (pointLightColor * pointWeight *  attenuation * pointDiffuse));

	vec3 pointReflectedLight = pointLightColor * pointSpecular * pointStrength * pointWeight;

	// don’t modulate the underlying color with reflected light,
	//   only with scattered light
	vec3 rgb;
	rgb = max(surfaceColor.rgb * ScatteredLight , (surfaceColor.rgb * vec3(0.05, 0.05, 0.05)));

	FragColor = vec4(rgb, 1.0);
	
}
