#version 330 core

layout(location = 0) in vec3 VertexPosition;
layout(location = 1) in vec2 in_tex_coord;
//layout(location = 1) in vec3 VertexColor;
layout(location = 2) in vec3 VertexNormal;

uniform mat4 PVMMatrix;
uniform mat4 MVMatrix;
uniform mat3 NormalMatrix; 	// to transform normals, pre-perspective
uniform vec3 EyePosition;
uniform vec3 LightPosition;


out vec2 vs_tex_coord;

//out vec3 Color;
out vec3 Normal;
out vec3 pointHalfVector;
out vec4 Position;

void main()
{
	
	vs_tex_coord = in_tex_coord;
	//Color = VertexColor;
	Normal = normalize(NormalMatrix * VertexNormal);
	Position = MVMatrix * vec4(VertexPosition, 1.0);

	vec3 EyeDirection = normalize(VertexPosition - EyePosition);
	
	vec3 pointLightDirection = LightPosition - vec3(Position);
	float lightDistance = length(pointLightDirection);
	pointLightDirection = pointLightDirection / lightDistance;
	
	vec3 pointHalfVector = normalize(pointLightDirection + EyeDirection);
	
	

	gl_Position = PVMMatrix * vec4(VertexPosition, 1.0);
}
