#version 330 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout (location = 2) in vec2 in_tex_coord;

out vec3 Normal;
out vec3 FragPos;
out vec2 vs_tex_coord;


uniform mat4 PVM;
uniform mat4 model;

void main()
{
	gl_Position = PVM * vec4(position, 1.0);
	FragPos = vec3(model * vec4(position, 1.0f));
	Normal = mat3(transpose(inverse(model))) * normal;
	vs_tex_coord = in_tex_coord;
}
