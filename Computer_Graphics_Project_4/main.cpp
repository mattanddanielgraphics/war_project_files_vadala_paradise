/*Daniel Vadala
* 10/27/17
* Computer Graphics
* Models in Space
* main.cpp
* main starter code from Professor Stuetzles example and modified by Daniel Vadala
*/




// User Includes

#include <ctime>

#include <cmath>

#include "Shader.hpp"
#include "Texture.hpp"
#include "Camera.h"
#include "Triangle.hpp"
#include "Quad.h"
#include "Card.h"
#include "Game.h"
#include "Table.h"
#include "Player.h"
#include "Deck.h"
#include "Room.h"
#include "SkyBox.h"
#include "PointLight.h"
#include "AmbientLight.hpp"



using std::endl;
using std::cerr;


//function declarations
void handleEvents(sf::Window& window);
void init();

//initialize states of drawables and camera
int currentDrawable = 0;
int cameraState = 0;

int score = 0;

// Window information
GLuint winHeight = 720;
GLuint winWidth = 1080;

// The current mouse position
double deltaX, deltaY;
bool lButtonDown;
bool rButtonDown;

bool once = true;

//flags for moving camera and models
bool upFlag = false;	bool wFlag = false;
bool downFlag = false;	bool sFlag = false;
bool rightFlag = false;	bool dFlag = false;
bool leftFlag = false;	bool aFlag = false;
bool playingFlag = false;


//lighting
vec3 lightPos = vec3(30, 30, 30);


int flipCounter = 0;

vec2 mousePos;


//declare objects
Texture* STZL;
Texture* KSL;


Table* newTable;

ScoreKeeper* scoreBoard;


Player* player1;
Player* player2;

Deck* masterDeck;

Game war;

SkyBox* room;

Shader* s;
Shader* lightShader;
Shader* lampShader;

PointLight* lamp;
AmbientLight* ambLight;

Camera* c;







int main() {
   
	war.setUpGame(8, 3, 3,
		"WAR!", winWidth, winHeight);
	war.setCallback(handleEvents);
	init();
	
    war.setShader(s);
    war.setCamera(c);

	war.addPlayers(player1, player2);
	war.addScoreBoard(scoreBoard);

	war.playGame();

	


    return 0;
}

void init() {


	//declare colors
	vec3 col1 = vec3(1.0f, 1.0f, 0.0f);
	vec3 col2 = vec3(1.0f, 1.0f, 0.0f);

	
	// Set up the shader
	string shaders[] = { "lightVertices.vert", "lightFragments.frag" };
	s = new Shader(shaders, true);
	//string lightShaders[] = { "lighting.vert", "lighting.frag" };
	//lightShader = new Shader(lightShaders, true);

	
	

	//string lampShaders[] = { "Lamp.vert", "Lamp.frag" };
	//lampShader = new Shader(lampShaders, true);




	// Set up the camera
	vec3 pos(40, 40, 100.0);
	GLfloat FOV = 45.0f;
	GLfloat nearPlane = 0.1f;
	GLfloat farPlane = 1000.0f;
	c = new Camera(pos, winWidth, winHeight);
	c->setPerspective(FOV, (GLfloat)winWidth / (GLfloat)winHeight,
		nearPlane, farPlane);
	
	
	
	int tableHeight = 4.2;

	STZL = new Texture("Cards/stuetzle.png");
	KSL = new Texture("Cards/kissel.png");
	masterDeck = new Deck(vec3(5, 4.2, 55));

	player1 = new Player(vec3(0.0, 20.0, 0.0), vec3(10, 4.2, 45), masterDeck->splitDeck(1), 1, STZL, -4);
	player2 = new Player(vec3(60.0, 20.0, -4.0), vec3(70, 4.2, 45), masterDeck->splitDeck(2), 2, KSL, 4);


	scoreBoard = new ScoreKeeper(vec3(0.0, 15.0, 0.0));
	newTable = new Table(0.0f, 4.0f, 80.0f, 60.0f, 30.0f, 3.0f);


	Texture* wallText = new Texture("wall.png");
	Texture* ceilingText = new Texture("ceiling.png");
	Texture* floorText = new Texture("carpet.png");

	room = new SkyBox(vec3(-35, -30, -20), 150, vec3(1, 1, 1), vec3(1, 1, 1));
	room->setTextures(wallText, ceilingText, floorText);
	
	scoreBoard->setPlayer1Score(player1->getScore());
	scoreBoard->setPlayer2Score(player2->getScore());
	
	ambLight = new AmbientLight(vec3(1.0, 1.0, 1.0));
	ambLight->enable();
	lamp = new PointLight(vec3(0, 0, 1), vec3(40, 10, 45),  10.0, 1.0, 0.05, 0.0, 1.0);
	lamp->enable();
	war.addLight(ambLight);
	war.addLight(lamp);
	// Finally, tell the application object about the drawable objects
	for (int i = 0; i < 52; i++)
	{
		war.addDrawableObject(masterDeck->getCardAt(i));
	}


	war.addDrawableObject(newTable);
	war.addDrawableObject(player1);
	war.addDrawableObject(player2);
	war.addDrawableObject(scoreBoard);
	war.addDrawableObject(room);

}



//handle user events
void handleEvents(sf::Window& window) {
	sf::Event event;

	// While there are still events.
	while (window.pollEvent(event)) {
		if (event.type == sf::Event::Closed) {
			window.close();
		}
		else if (event.type == sf::Event::Resized) {
			winHeight = event.size.height;
			winWidth = event.size.width;
			glViewport(0, 0, winWidth, winHeight);
		}// Keyboard pressed
		else if (event.type == sf::Event::KeyPressed)
		{
			//flags for movement
			if (event.key.code == sf::Keyboard::W) {
				wFlag = true;
			}
			if (event.key.code == sf::Keyboard::A) {
				aFlag = true;
			}
			if (event.key.code == sf::Keyboard::S) {
				sFlag = true;
			}
			if (event.key.code == sf::Keyboard::D) {
				dFlag = true;
			}
			if (event.key.code == sf::Keyboard::Up) {
				upFlag = true;
			}
			if (event.key.code == sf::Keyboard::Down) {
				downFlag = true;
			}
			if (event.key.code == sf::Keyboard::Left) {
				leftFlag = true;
			}
			if (event.key.code == sf::Keyboard::Right) {
				rightFlag = true;
			}
		}
		else if (event.type == sf::Event::KeyReleased) {
			if (event.key.code == sf::Keyboard::Q) {
				window.close();
			}

			if (event.key.code == sf::Keyboard::N) {
				if (!war.isGameOver())
				{
					if (!playingFlag)
					{
						war.playTurn();
						playingFlag = true;

					}
					else if(playingFlag)
					{
						war.finishTurn();
						playingFlag = false;
					}

				}

			}
			if (event.key.code == sf::Keyboard::T)
			{
				ambLight->toggle();
			}
			if (event.key.code == sf::Keyboard::Y)
			{
				lamp->toggle();
			}
			if (event.key.code == sf::Keyboard::W) {
				wFlag = false;
				player1->scale(2);
				player2->scale(2);
				
			}
			if (event.key.code == sf::Keyboard::A) {
				aFlag = false;
			}
			if (event.key.code == sf::Keyboard::S) {
				sFlag = false;
			}
			if (event.key.code == sf::Keyboard::D) {
				dFlag = false;
			}
			if (event.key.code == sf::Keyboard::Up) {
				upFlag = false;
			}
			if (event.key.code == sf::Keyboard::Down) {
				downFlag = false;
			}
			if (event.key.code == sf::Keyboard::Left) {
				leftFlag = false;
			}
			if (event.key.code == sf::Keyboard::Right) {
				rightFlag = false;
			}

		}            // Mouse button pressed  
		else if (event.type == sf::Event::MouseButtonReleased) {
			if (event.mouseButton.button == sf::Mouse::Left) {
				lButtonDown = false;
			}
			else if (event.mouseButton.button == sf::Mouse::Right) {
				rButtonDown = false;
			}
		}
		else if (event.type == sf::Event::MouseButtonPressed) {
			if (event.mouseButton.button == sf::Mouse::Left) {
				lButtonDown = true;
			}
			else if (event.mouseButton.button == sf::Mouse::Right) {
				rButtonDown = true;
			}
		}
		else if (event.type == sf::Event::MouseMoved) {
			sf::Vector2i m = sf::Mouse::getPosition();
			deltaX = mousePos.x - m.x;
			deltaY = mousePos.y - m.y;
			mousePos.x = m.x;
			mousePos.y = m.y;

			// If the associated button is down, make sure to 
			//   update the camera accordingly.
			if (lButtonDown) {
				
				c->setViewByMouse(deltaX, deltaY);
			}
			if (rButtonDown) {
				// This is negative BECAUSE THE LOOK VECTOR IS BACKWARDS...hack
				c->moveCamera(-deltaX, deltaY);
			}
		}
		//check movement flags
		if (upFlag)
		{
			lamp->moveLight(vec3(0, 0, -3), s);
		}
		if (downFlag)
		{
			lamp->moveLight(vec3(0, 0, 3), s);
		}
		if (leftFlag)
		{
			lamp->moveLight(vec3(-3, 0, 0), s);

		}
		if (rightFlag)
		{
			lamp->moveLight(vec3(3, 0, 0), s);
		}
		if (wFlag)
		{

		}
		if (aFlag)
		{

		}
		if (sFlag)
		{

		}
		if (dFlag)
		{


		}
		
		
	}
}
